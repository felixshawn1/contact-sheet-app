package sra.data;

import java.util.Comparator;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/*
 * Class to contain information about a single person pulled from a contact sheet.
 */
public class Contact implements Comparator<Contact>, Comparable<Contact> {

	private StringProperty firstName;
	private StringProperty lastName;
	private StringProperty middleName;
	private StringProperty title;
	
	
	public Contact() {
		firstName = new SimpleStringProperty();
		lastName = new SimpleStringProperty();
		middleName = new SimpleStringProperty();
		title = new SimpleStringProperty();
	}
	@Override
	public int compareTo(Contact post){
		// TODO design and implement comparing algorithm
		
		return 0;
	}
	
	@Override
	public int compare(Contact pre, Contact post) {
		// TODO design and implement comparing algorithm
		
		return 0;
	}
	
	public String getFirstName() {
		return firstName.get();
	}

	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}


	public String getLastName() {
		return lastName.get();
	}


	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}


	public String getMiddleName() {
		return middleName.get();
	}


	public void setMiddleName(String middleName) {
		this.middleName.set(middleName);
	}


	public String getTitle() {
		return title.get();
	}


	public void setTitle(StringProperty title) {
		this.title = title;
	}

	


	
}
