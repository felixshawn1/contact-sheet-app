package sm.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * Test connection to the database server that was successful. 
 * follow this guide for local installation: 
 * http://www.ccs.neu.edu/home/kathleen/classes/cs3200/MySQLWindows.pdf
 * 
 */
public class DBTest {

	
	
	public static void main(String[] args) {
		try {
			Connection conn = null;
			
			//Class.forName("com.mysql.jdbc.Driver").newInstance();
			
			conn = DriverManager.getConnection("jdbc:mysql://localhost/okd" +
				                                   "user=root&password=admin");
			Statement stmt = null;
			try {
			stmt = conn.createStatement();
			stmt.executeUpdate("create table " + "test" +
			        ".SUPPLIERS " +
			        "(SUP_ID integer NOT NULL, " +
			        "SUP_NAME varchar(40) NOT NULL, " +
			        "STREET varchar(40) NOT NULL, " +
			        "CITY varchar(20) NOT NULL, " +
			        "STATE char(2) NOT NULL, " +
			        "ZIP char(5), " +
			        "PRIMARY KEY (SUP_ID))");
			} catch(SQLException ex) {
				ex.printStackTrace();
			}	finally {
				if (stmt != null) { stmt.close(); }
			}
			
		} catch(SQLException e) {
			// handle any errors
		    System.out.println("SQLException: " + e.getMessage());
		    System.out.println("SQLState: " + e.getSQLState());
		    System.out.println("VendorError: " + e.getErrorCode());
		}
	}
}
